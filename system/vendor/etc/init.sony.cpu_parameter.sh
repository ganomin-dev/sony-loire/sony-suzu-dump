#!/vendor/bin/sh
# *********************************************************************
# * Copyright 2016 (C) Sony Mobile Communications Inc.                *
# * All rights, including trade secret rights, reserved.              *
# *********************************************************************
#

# set cpu_boost parameters
echo "0:1305600" > /sys/module/cpu_boost/parameters/input_boost_freq
echo "40" > /sys/module/cpu_boost/parameters/input_boost_ms
echo 9 > /proc/sys/kernel/sched_upmigrate_min_nice

# set (super) packing parameters
echo 1017600 > /sys/devices/system/cpu/cpu0/sched_mostly_idle_freq
echo 0 > /sys/devices/system/cpu/cpu4/sched_mostly_idle_freq

# Ensure 512 kb read-ahead setting
echo 512 > /sys/block/dm-0/queue/read_ahead_kb
echo 512 > /sys/block/dm-1/queue/read_ahead_kb
echo 512 > /sys/block/mmcblk0/queue/read_ahead_kb

# disallow upmigrate for cgroup's tasks
echo 1 > /dev/cpuctl/bg_non_interactive/cpu.upmigrate_discourage

# Changes to optimize performance and power consumption
echo 200000 > /proc/sys/kernel/sched_freq_inc_notify

exit 0
